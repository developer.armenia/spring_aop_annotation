package am.egs.service;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

/**
 * Created by haykh on 4/29/2019.
 */
@Component
@Aspect
@EnableAspectJAutoProxy
public class AopForLog {
  @Before("execution(public void doSomething())")
  public void log(){
    System.out.println("Hello from out of method, from AOP");
  }
}
